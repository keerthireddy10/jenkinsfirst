package com.amazon.testbase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import com.amazon.utils.TestUtil;

public class TestBase {

	
	public static WebDriver driver;
	public static Properties prop;
	
	public TestBase()
	{
	
		try {
			prop = new Properties();
			FileInputStream fis= new FileInputStream("C:\\Users\\LENOVO\\eclipse-workspace1\\Amazon\\src\\main\\java\\com\\amazon\\configuration\\config.properties");
			prop.load(fis);
		   }catch(FileNotFoundException e) {
			   e.printStackTrace();
		   }catch(IOException e) {
			   e.printStackTrace(); 
		   }	
		
	}
	
	public static void initialization()
	{
		String browserName= prop.getProperty("browser");
		
		if(browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:\\selenium software\\chromedriver_win32\\chromedriver.exe");
	         driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("url"));
	}
	
}
