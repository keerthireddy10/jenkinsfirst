package com.amazon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.amazon.testbase.TestBase;

public class SelectApple extends TestBase{
	
	@FindBy(xpath = "//a[contains(text(),'Computers')]")
    public  WebElement computers;

	@FindBy(xpath = "//a[@href='/s/ref=mega_elec_s23_2_1_2_3?rh=i%3Acomputers%2Cn%3A10559548031&ie=UTF8&bbn=976392031']")
	public WebElement appletitle;
	
	@FindBy(xpath = "//span[contains(text(),'Laptops & Accessories')]")
    public  WebElement lapAndacc; 
	
	@FindBy(xpath = "(//h2[@class='a-size-mini a-color-base apb-line-clamp-3 a-text-normal'])[1]")
    public  WebElement applelaptop; 
	
	public SelectApple()
	{
	   PageFactory.initElements(driver,this);
	}
	
	
	public void appleBrand()
	{
		computers.click();
		Actions actions = new Actions(driver);
		actions.moveToElement(lapAndacc).build().perform();
		actions.moveToElement(appletitle).click().build().perform();
		applelaptop.click();
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='add-to-cart-button']")));
	}
}
