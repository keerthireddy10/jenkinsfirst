package com.amazon.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.testbase.TestBase;

public class ViewCart extends TestBase{

	
	@FindBy(xpath = "//span[@id='nav-cart-count']")
    public  WebElement viewcart;
	
	
	public ViewCart()
	{
		PageFactory.initElements(driver, this);
	}
}

