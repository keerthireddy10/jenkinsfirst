package com.amazon.pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.amazon.testbase.TestBase;

public class SelectHpBrand extends TestBase {
	
	@FindBy(xpath = "//a[contains(text(),'Computers')]")
    public  WebElement computers;
	
	@FindBy(xpath = "//span[contains(text(),'Laptops & Accessories')]")
    public  WebElement lapandacc; 
	
	@FindBy(xpath = "//img[@src='https://images-na.ssl-images-amazon.com/images/G/31/img21/PC/Computers/Revamp/SBB/xcm_banners_02_sbb_v1_564x564_in-en._SS400_QL85_.jpg']")
	public WebElement hpshop;
	
	@FindBy(xpath = "//span[contains(text(),'HP 15 (2021) Thin & Light Ryzen 3-3250 Laptop, 8 GB RAM, 1TB HDD + 256GB SSD, 15.6\" (39.2 cms) FHD Screen, Windows 10, MS Office (15s-gr0012AU)')]")
	public WebElement  hplaptop;
	
	
	
	public SelectHpBrand()
	{
	   PageFactory.initElements(driver,this);
	}

	
	
	public void verifybrand()
	{
		computers.click();
		lapandacc.click();
		hpshop.click();
		hplaptop.click();
		
		String parent = driver.getWindowHandle();
        Set<String> s = driver.getWindowHandles();
        // Now iterate using Iterator
        Iterator<String> I1 = s.iterator();
       while (I1.hasNext()) {
            String child_window = I1.next();

            if (!parent.equals(child_window)) {
                driver.switchTo().window(child_window);
                WebDriverWait wait = new WebDriverWait(driver,10);
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='add-to-cart-button']")));
                
                
                //System.out.println(driver.switchTo().window(child_window).getTitle());
                //driver.close();
            }
             }
		
	}
}
