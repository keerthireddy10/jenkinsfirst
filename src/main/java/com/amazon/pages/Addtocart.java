package com.amazon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.amazon.testbase.TestBase;

public class Addtocart extends TestBase{
	
	@FindBy(xpath = "//input[@id='add-to-cart-button']")
	public WebElement addcart;
	
	public Addtocart()
	{
	   PageFactory.initElements(driver,this);
	}
	
	public void verifyCart()
	{
		addcart.click();
		WebDriverWait wait = new WebDriverWait(driver,10);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='attach-added-to-cart-alert-and-image-area']")));
		
	}
	

}
