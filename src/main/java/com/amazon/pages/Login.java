package com.amazon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.amazon.testbase.TestBase;

public class Login extends TestBase{

	
	@FindBy(xpath = "//span[@id='nav-link-accountList-nav-line-1']")
    public  WebElement hello;
	
	@FindBy(xpath = "//span[@class='nav-action-inner']")
    public  WebElement Signin;
	
	@FindBy(xpath = "//a[@id='nav-logo-sprites']")
    public WebElement logo;
	
	@FindBy(xpath = "//input[@name='email']")
    public  WebElement email;
	
	@FindBy(xpath = "//input[@id='continue']")
    public  WebElement conbtn;
	
	@FindBy(xpath = "//input[@id='ap_password']")
    public  WebElement password;
	
	@FindBy(xpath = "//input[@id='signInSubmit']")
    public  WebElement signbtn;
	
	
	
	public Login()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public  void verifyLoginSuccess()
	{
		
		
		try {
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='nav-logo-sprites']")));
		
		}
		catch(Exception e)
		{
			System.out.println(e);
			System.out.println(" invalid credentials");
		}
		
	}
}
