package com.amazon.testcases;

import java.io.IOException;


import org.openqa.selenium.interactions.Actions;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.amazon.pages.Login;
import com.amazon.testbase.TestBase;
import com.amazon.utils.TestUtil;


public class TC_001LoginTest  extends TestBase{
	
	Login lpobj;
	
	public TC_001LoginTest()
	{
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
	    
	}
	
	
	@Test(priority =1, dataProvider="Data")
	public void verifylogin(String username,String password) throws InterruptedException
	{
	
		lpobj = new Login();
		Actions actions = new Actions(driver);
		actions.moveToElement(lpobj.hello).build().perform();
		actions.moveToElement(lpobj.Signin).click().build().perform();
		lpobj.email.clear();
		lpobj.email.sendKeys(username);
		lpobj.conbtn.click();
		lpobj.password.clear();
		lpobj.password.sendKeys(password);
		lpobj.signbtn.click();
		lpobj.verifyLoginSuccess();
		
	}
	
	@DataProvider(name="Data")
	public String [][] getData() throws IOException
	{
		
		
		//get the data from excel
		String path="C:\\Users\\LENOVO\\eclipse-workspace1\\Amazon\\src\\main\\java\\com\\amazon\\testdata\\AmazonHackathon.xlsx";
		String sheetName = "Sheet1";
		TestUtil util=new TestUtil(path,sheetName);
		
		int totalrows=util.getRowCount("Sheet1");
		int totalcols=util.getCellCount("Sheet1",1);	
				
		String loginData[][]=new String[totalrows][totalcols];
			
		
		for(int i=1;i<=totalrows;i++) //1
		{
			for(int j=0;j<totalcols;j++) //0
			{
				loginData[i-1][j]=util.getCellData("Sheet1", i, j);
			}
				
		}
		
		return loginData;
	}
	 @AfterMethod
	 public void teardown()
	 {
		 driver.quit();
	 }
}
