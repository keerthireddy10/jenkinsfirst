package com.amazon.testcases;

import java.io.IOException;

import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.amazon.pages.Addtocart;
import com.amazon.pages.Login;
import com.amazon.pages.SelectHpBrand;
import com.amazon.testbase.TestBase;
import com.amazon.utils.TestUtil;

public class TC_003HpAddToCart extends TestBase{

Login lpobj;
SelectHpBrand sbobj;
Addtocart cobj;
	
	public TC_003HpAddToCart()
	{
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
	    
	}
	
	@Test(priority =1, dataProvider="Data")
	public void verifyHpAddToCart(String username,String password) throws InterruptedException
	{
	
		lpobj = new Login();
		Actions actions = new Actions(driver);
		actions.moveToElement(lpobj.hello).build().perform();
		actions.moveToElement(lpobj.Signin).click().build().perform();
		lpobj.email.clear();
		lpobj.email.sendKeys(username);
		lpobj.conbtn.click();
		lpobj.password.clear();
		lpobj.password.sendKeys(password);
		lpobj.signbtn.click();
		lpobj.verifyLoginSuccess();
		sbobj = new SelectHpBrand();
		sbobj.verifybrand();
		cobj= new Addtocart();
		cobj.verifyCart();
		
	}
	
	@DataProvider(name="Data")
	public String [][] getData() throws IOException
	{
		
		
		//get the data from excel
		String path="C:\\Users\\LENOVO\\eclipse-workspace1\\Amazon\\src\\main\\java\\com\\amazon\\testdata\\AmazonHackathon.xlsx";
		String sheetName = "Sheet1";
		TestUtil util=new TestUtil(path,sheetName);
		
		int totalrows=util.getRowCount("Sheet1");
		int totalcols=util.getCellCount("Sheet1",1);	
				
		String loginData[][]=new String[1][totalcols];
			
		
		for(int i=1;i<=1;i++) //1
		{
			for(int j=0;j<totalcols;j++) //0
			{
				loginData[i-1][j]=util.getCellData("Sheet1", i, j);
			}
				
		}
		
		return loginData;
	}
	 @AfterMethod
	 public void teardown()
	 {
		 driver.quit();
	 }

}
